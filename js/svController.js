function layThongTinTuForm() {
  // lấy thông tin từ user
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var diemToan = document.getElementById("txtDiemToan").value * 1;
  var diemLy = document.getElementById("txtDiemLy").value * 1;
  var diemHoa = document.getElementById("txtDiemHoa").value * 1;

  var sv = new SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa);
  return sv;
}

function renderDssv(list) {
  // render danh sách
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var currentSv = list[i];
    var contentTr = `<tr>
    <td>${currentSv.ma}</td>
    <td>${currentSv.ten}</td>
    <td>${currentSv.email}</td>
    <td>${currentSv.tinhDTB()}</td>
    <td>
    <button onclick = "xoaSv('${
      currentSv.ma
    }')" class="btn btn-danger">Xoá</button>
    <button onclick="suaSv('${
      currentSv.ma
    }')" class="btn btn-primary">Sửa</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

function resetForm() {
  // RESET FORM ~ khi HTML có thẻ form mới reset được theo kiểu :
  document.getElementById("formQLSV").reset();
}
