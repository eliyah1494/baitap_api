const BASE_URL = "https://633ec04c83f50e9ba3b7605d.mockapi.io";

var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};
// lấy danh sách sinh viên service
var fetchDssvService = function () {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      renderDanhSachSinhVien(res.data);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
};
// chạy lần đầu khi load trang
fetchDssvService();
//   render dssv
var renderDanhSachSinhVien = function (listSv) {
  var contentHTML = "";
  listSv.forEach(function (sv) {
    contentHTML += `<tr>
        <td>${sv.ma}</td>
        <td>${sv.ten}</td>
        <td>${sv.email}</td>
        <td>${tinhDTB(sv)}</td>
        <td>
        <button onclick="layThongTinChiTietSv(${
          sv.ma
        })" class="btn btn-primary">Sửa</button>
        <button onclick="xoaSv(${
          sv.ma
        })" class="btn btn-danger">Xoá</button></td>

        </tr>`;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};
var xoaSv = function (idSv) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${idSv}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      fetchDssvService();
      Swal.fire("Xoá thành công");
    })
    .catch(function (err) {
      tatLoading();
      Swal.fire("Xoá thất bại");
    });
};

var themSv = function () {
  var sv = layThongTinTuForm();
  console.log("sv: ", sv);

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      Swal.fire("Thêm thành công");
      fetchDssvService();
    })
    .catch(function (err) {
      Swal.fire("Thêm thất bại");
    });
};

var layThongTinChiTietSv = function (idSv) {
  // axios({
  //   url: `${BASE_URL}/sv/${idSv}`,
  //   method: "PUT",
  // });

  axios
    .get(`${BASE_URL}/sv/${idSv}`)
    .then(function (res) {
      console.log("res: ", res.data);
      // gọi hàm show thông tin lên form
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
  document.getElementById("txtMaSV").disabled = true;
};

var capNhatSv = function () {
  batLoading();
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      tatLoading();
      sv = res.data;
      Swal.fire("Sửa thành công");
      fetchDssvService();
    })
    .catch(function (err) {
      tatLoading();
      Swal.fire("Sửa thất bại");
    });
  resetForm();
};

function tinhDTB(sv) {
  var diemTB = (sv.diemToan * 1 + sv.diemLy * 1 + sv.diemHoa * 1) / 3;
  return diemTB.toFixed(1);
}
